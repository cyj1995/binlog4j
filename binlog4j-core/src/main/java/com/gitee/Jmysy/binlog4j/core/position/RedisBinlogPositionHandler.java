package com.gitee.Jmysy.binlog4j.core.position;

import com.alibaba.fastjson.JSON;
import com.gitee.Jmysy.binlog4j.core.config.RedisConfig;
import com.gitee.Jmysy.binlog4j.core.exception.Binlog4jException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

@Slf4j
public class RedisBinlogPositionHandler implements BinlogPositionHandler {

    private final JedisPool jedisPool;

    public RedisBinlogPositionHandler(RedisConfig redisConfig) {
        this.jedisPool = new JedisPool(new GenericObjectPoolConfig<>(), redisConfig.getHost(), redisConfig.getPort(), 1000, redisConfig.getPassword(), redisConfig.getDatabase());
    }

    @Override
    public BinlogPosition loadPosition(Long serverId) {
        try (Jedis jedis = jedisPool.getResource()) {
            String value = jedis.get(serverId.toString());
            if (value != null) {
                return JSON.parseObject(value, BinlogPosition.class);
            }
        } catch (JedisConnectionException e) {
            throw new Binlog4jException("Unable to connect to Redis host.");
        }
        return null;
    }

    @Override
    public void savePosition(BinlogPosition position) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(position.getServerId().toString(), JSON.toJSONString(position));
        }
    }
}
