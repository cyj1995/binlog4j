package com.gitee.Jmysy.binlog4j.core.handler;

import com.gitee.Jmysy.binlog4j.core.BinlogEvent;
import com.gitee.Jmysy.binlog4j.core.GenericBinlogEventHandler;

public class SuperBinlogEventHandler extends GenericBinlogEventHandler {

    @Override
    public void onInsert(BinlogEvent event) {
        System.out.println("插入事件:" + event.getData());
    }

    @Override
    public void onUpdate(BinlogEvent event) {
        System.out.println("修改事件:" + event.getData());
    }

    @Override
    public void onDelete(BinlogEvent event) {
        System.out.println("删除事件:" + event.getData());
    }

    @Override
    public boolean isHandle(String database, String table) {
        return database.equals("pear-admin") && table.equals("user");
    }
}
